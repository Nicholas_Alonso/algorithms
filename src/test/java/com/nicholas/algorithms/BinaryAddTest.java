package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
/**
 * Created by Nicholas on 1/4/15.
 */
public class BinaryAddTest {

    @Test
    public void testBinaryAdd() {

        List<Integer> addResult = BinaryAdd.binAdd(Arrays.asList(1, 0, 1, 0), Arrays.asList(0, 1, 1, 1));
        assertEquals(addResult,Arrays.asList(1, 0, 0, 0, 1));
    }
}
