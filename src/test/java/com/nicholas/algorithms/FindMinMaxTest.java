package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

public class FindMinMaxTest {

    @Test
    public void testMin() {

        assertEquals(2, (int)FindMinMax.minimum(Arrays.asList(5, 3, 2, 7, 6, 8, 11)));
    }

    @Test
    public void testMax() {

        assertEquals(11, (int)FindMinMax.maximum(Arrays.asList(5, 3, 2, 7, 6, 8, 11)));
    }
}