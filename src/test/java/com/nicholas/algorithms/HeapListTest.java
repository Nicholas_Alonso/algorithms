package com.nicholas.algorithms;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class HeapListTest {

    @Test
    public void testMaxHeapify() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(12);
        myArray.add(18);
        myArray.add(10);
        myArray.add(8);
        myArray.add(5);
        myArray.add(3);
        myArray.add(2);
        HeapList<Integer> heapA = new HeapList<>(myArray);
        heapA.maxHeapify(0);
        assertEquals(Arrays.asList(18, 12, 10, 8, 5, 3, 2), heapA.getHeapList());
    }

    @Test
    public void testMinHeapify() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(3);
        myArray.add(2);
        myArray.add(5);
        myArray.add(8);
        myArray.add(10);
        myArray.add(12);
        myArray.add(18);
        HeapList<Integer> heapB = new HeapList<>(myArray);
        heapB.minHeapify(0);
        assertEquals(Arrays.asList(2, 3, 5, 8, 10, 12, 18), heapB.getHeapList());
    }

    @Test
    public void testBuildMaxHeap() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(5);
        myArray.add(8);
        myArray.add(10);
        myArray.add(12);
        myArray.add(18);
        myArray.add(3);
        myArray.add(2);
        HeapList<Integer> heapC = new HeapList<>(myArray);
        heapC.buildMaxHeap();
        assertEquals(Arrays.asList(18, 12, 10, 5, 8, 3, 2), heapC.getHeapList());
    }

    @Test
    public void testBuildMinHeap() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(5);
        myArray.add(8);
        myArray.add(10);
        myArray.add(12);
        myArray.add(18);
        myArray.add(3);
        myArray.add(2);
        HeapList<Integer> heapD = new HeapList<>(myArray);
        heapD.buildMinHeap();
        assertEquals(Arrays.asList(2, 8, 3, 12, 18, 5, 10), heapD.getHeapList());
    }

    @Test
    public void testHeapListSort() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(5);
        myArray.add(8);
        myArray.add(10);
        myArray.add(12);
        myArray.add(18);
        myArray.add(3);
        myArray.add(2);
        HeapList<Integer> heapE = new HeapList<>(myArray);
        assertEquals(Arrays.asList(2, 3, 5, 8, 10, 12, 18), heapE.heapListSort());
    }

    @Test

    public void testMaxHeapInsert() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(18);
        myArray.add(12);
        myArray.add(10);
        myArray.add(5);
        myArray.add(8);
        myArray.add(3);
        myArray.add(2);
        HeapList<Integer> heapF = new HeapList<>(myArray);
        heapF.maxHeapInsert(6);
        assertEquals(Arrays.asList(18, 12, 10, 6, 8, 3, 2 , 5), heapF.getHeapList());
    }

    @Test
    public void testMinHeapInsert() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(2);
        myArray.add(8);
        myArray.add(3);
        myArray.add(12);
        myArray.add(18);
        myArray.add(5);
        myArray.add(10);
        HeapList<Integer> heapG = new HeapList<>(myArray);
        heapG.minHeapInsert(6);
        assertEquals(Arrays.asList(2, 6, 3, 8, 18, 5, 10, 12), heapG.getHeapList());
    }

    @Test
    public void testMaxHeapDelete() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(18);
        myArray.add(12);
        myArray.add(10);
        myArray.add(5);
        myArray.add(8);
        myArray.add(3);
        myArray.add(2);
        HeapList<Integer> HeapH = new HeapList<>(myArray);

        HeapH.maxHeapDelete(0);

        assertEquals(Arrays.asList(12, 8, 10, 5, 2, 3), HeapH.getHeapList());
    }

    @Test
    public void testMinHeapDelete() {
        ArrayList<Integer> myArray = new ArrayList<>();
        myArray.add(2);
        myArray.add(8);
        myArray.add(3);
        myArray.add(12);
        myArray.add(18);
        myArray.add(5);
        myArray.add(10);
        HeapList<Integer> heapI = new HeapList<>(myArray);

        heapI.minHeapDelete(0);

        assertEquals(Arrays.asList(3, 8, 5, 12, 18, 10), heapI.getHeapList());
    }
}