package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

public class BucketSortTest {
    @Test
    public void testBucketSort() {

        assertEquals(Arrays.asList(0.11, 0.13, 0.25, 0.51, 0.92), BucketSort.bSort(Arrays.asList(0.92, 0.51, 0.13, 0.11,0.25)) );
    }
}