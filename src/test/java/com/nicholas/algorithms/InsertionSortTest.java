package com.nicholas.algorithms;

import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class InsertionSortTest {

    @Test
    public void testInsertionSort() {

        List<Integer> listToSort = new ArrayList<>(Arrays.asList(5, 3, 2, 1, 4));
        List<Integer> sorted = new ArrayList<>(listToSort);

        Collections.sort(sorted);

//      assertEquals(Arrays.asList(1, 2, 3, 4, 5),
        assertEquals(sorted, InsertionSort.iSort(listToSort));
    }
}