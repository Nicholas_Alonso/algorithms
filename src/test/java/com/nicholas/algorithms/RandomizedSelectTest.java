package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

public class RandomizedSelectTest {

    @Test
    public void testRandomizedSelect() {
        assertEquals(5, (int)RandomizedSelect.randomizedSelect(Arrays.asList(3, 1, 2, 5, 1, 9), 0, 5, 5));
    }
}