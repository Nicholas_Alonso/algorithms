package com.nicholas.algorithms;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class CountingSortTest {

    @Test
    public void testCountingSort() {

        int[] sorted = new int[5];

        CountingSort.countingSort(new int[] {3, 4, 1, 2, 6}, sorted, 6);
        assertArrayEquals(new int[]{1, 2, 3, 4, 6}, sorted);
    }
}