package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

public class SelectionSortTest {

    @Test
    public void testSelectionSort() {
        assertEquals(Arrays.asList(1, 2, 3, 4, 5), SelectionSort.sSort(Arrays.asList(5, 3, 2, 1, 4)));
    }
}