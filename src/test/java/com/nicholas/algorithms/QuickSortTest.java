package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

public class QuickSortTest {

    @Test
    public void testQuickSort() {
        assertEquals(Arrays.asList(1, 2, 3, 4, 5, 17), QuickSort.quickSort(Arrays.asList(3, 2, 5, 4, 1, 17)));
    }
}