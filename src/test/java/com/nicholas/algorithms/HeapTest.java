package com.nicholas.algorithms;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class HeapTest {

    @Test
    public void testMaxHeapify() {

        Heap heapA = new Heap(new int[] {12, 18, 10, 8, 5, 3, 2});
        heapA.maxHeapify(0);

       assertArrayEquals(new int[]{18, 12, 10, 8, 5, 3, 2}, heapA.getHeapArray());
    }

    @Test
    public void testMinHeapify() {

        Heap heapB = new Heap(new int[] {3, 2, 5, 8, 10, 12, 18});
        heapB.minHeapify(0);

        assertArrayEquals(new int[] {2, 3, 5, 8, 10, 12, 18}, heapB.getHeapArray());
    }

    @Test
    public void testBuildMaxHeap() {

        Heap heapC = new Heap(new int[] {5, 8, 10, 12, 18, 3, 2});

        heapC.buildMaxHeap();
        assertArrayEquals(new int[] {18, 12, 10, 5, 8, 3, 2}, heapC.getHeapArray());
    }

    @Test
    public void testBuildMinHeap() {

        Heap heapD = new Heap(new int[] {5, 8, 10, 12, 18, 3, 2});

        heapD.buildMinHeap();
        assertArrayEquals(new int[] {2, 8, 3, 12, 18, 5, 10}, heapD.getHeapArray());
    }

    @Test
    public void testHeapSort() {

        Heap heapD = new Heap(new int[] {5, 8, 10, 12, 18, 3, 2});
        assertArrayEquals(new int[] {2, 3, 5, 8, 10, 12, 18}, heapD.heapSort());
    }
}