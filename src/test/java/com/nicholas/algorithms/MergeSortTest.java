package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

public class MergeSortTest  {

    @Test
    public void testMergeSort() {
        assertEquals(Arrays.asList(1, 4 ,3 , 2, 5), MergeSort.mergeSort(Arrays.asList(3, 2, 5, 4, 1)));
    }
}