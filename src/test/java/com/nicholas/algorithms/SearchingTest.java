package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

public class SearchingTest  {

    @Test
    public void testSearching() {
        assertEquals(1, Searching.search(5, Arrays.asList(1, 5, 3, 4, 2)));
    }
}