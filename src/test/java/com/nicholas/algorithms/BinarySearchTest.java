package com.nicholas.algorithms;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;
/**
 * Created by Nicholas on 1/4/15.
 */

public class BinarySearchTest {

    @Test
    public void testBinarySearch() {

        assertEquals(5, BinarySearch.binarySearch(5, Arrays.asList(0, 1, 2, 3, 4, 5)));
        assertEquals(4, BinarySearch.binarySearch(14, Arrays.asList(0, 1, 2, 3, 14, 25)));
    }
}