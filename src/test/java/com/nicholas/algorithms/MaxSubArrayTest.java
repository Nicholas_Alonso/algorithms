package com.nicholas.algorithms;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class MaxSubArrayTest {

    @Test
    public void testMaxSubArray() {
        assertArrayEquals(new int[]{0,7,18}, MaxSubArray.findMaxSubArray(new int[] {10, -5, 6, 5, -10, 2 ,4, 6, -5, 3},0,9 ));
    }
}