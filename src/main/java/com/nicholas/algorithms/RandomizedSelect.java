package com.nicholas.algorithms;

import java.util.List;
import java.util.Random;

/**
 * Uses the randomized partition method to determine the ith smallest
 * element of a list.
 *
 * @author Nicholas Alonso
 */
public class RandomizedSelect {
    /**
     * Changes the order of the list so that all elements
     * smaller than the element at index r are moved to the
     * left side of the list and then the element at index r
     * is swapped with the first element in the list that is
     * not less than it.
     *
     * @param listToSort The list that will be sorted
     * @param p          The lowest index of the list or sub list
     * @param r          The highest index of the list or sub list
     * @param <T>        The type of elements in the list
     * @return The index of the element that the list was partitioned
     * around
     */
    public static <T extends Comparable<? super T>> int partition(List<T> listToSort, int p, int r) {

        T x = listToSort.get(r);
        int i = p - 1;

        for (int j = p; j < r; j++) {
            if (listToSort.get(j).compareTo(x) < 0) {
                i++;
                T temp = listToSort.get(i);
                listToSort.set(i, listToSort.get(j));
                listToSort.set(j, temp);
            }
        }
        T temp2 = listToSort.get(i + 1);
        listToSort.set(i + 1, listToSort.get(r));
        listToSort.set(r, temp2);

        return i + 1;
    }

    /**
     * Uses the partition method to randomly partition a list by swapping
     * an element at a random index of the list to index r.
     *
     * @param listToPartition The list that is being partitioned
     * @param p               The lowest index of the list being partitioned
     * @param r               The highest index of the list being partitioned
     * @param <T>             The type of elements in the list being partitioned
     * @return The index of that element that the list was
     * partitioned around
     */
    public static <T extends Comparable<? super T>> int randomizedPartition(List<T> listToPartition, int p, int r) {

        Random randNum = new Random();
        int index = randNum.nextInt(r - p) + p;
        T temp = listToPartition.get(index);
        listToPartition.set(index, listToPartition.get(r));
        listToPartition.set(r, temp);

        return partition(listToPartition, p, r);
    }

    /**
     * Given a list and an integer i randomizedSelect will determine the ith
     * smallest element of the list using randomizedPartition.
     *
     * @param myList The list that will be searched
     * @param p      The lowest index of myList
     * @param r      The highest index of myList
     * @param i      Randomized select will determine the ith smallest element
     * @param <T>    The type of elements in myList
     * @return The ith smallest element
     */
    public static <T extends Comparable<? super T>> T randomizedSelect(List<T> myList, int p, int r, int i) {

        if (p == r) {
            return myList.get(p);
        }
        int q = randomizedPartition(myList, p, r);
        int k = q - p + 1;

        if (i == k) {
            return myList.get(q);
        } else if (i < k) {
            return randomizedSelect(myList, p, q - 1, i);
        } else return randomizedSelect(myList, q + 1, r, i - k);
    }
}


