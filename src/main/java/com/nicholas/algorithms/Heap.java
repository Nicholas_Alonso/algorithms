package com.nicholas.algorithms;

/**
 * Created by Nicholas on 11/28/2014.
 */
public class Heap {

    private int heapSize;
    private int[] heapArray;

    public Heap(int[] myArray) {
        heapArray = myArray;
        heapSize = myArray.length;
    }

    public Heap() {
    }

    public int[] getHeapArray() {
        return heapArray;
    }

    public void setHeap(int arrayLength) {
        heapArray = new int[arrayLength];
    }

    public void setNode(int index, int value) {
        heapArray[index] = value;
    }

    public int getLeftChild(int index) {
        return ((index + 1) * 2) - 1;
    }

    public int getRightChild(int index) {
        return ((index + 1) * 2);
    }

    public int getParent(int index) {
        return (index - 1) / 2;
    }

    public void maxHeapify(int index) {

        int leftChild = getLeftChild(index);
        int rightChild = getRightChild(index);
        int largest;

        if (leftChild < heapSize && heapArray[leftChild] > heapArray[index]) {
            largest = leftChild;
        } else largest = index;

        if (rightChild < heapSize && heapArray[rightChild] > heapArray[largest]) {
            largest = rightChild;
        }
        if (largest != index) {
            int temp = heapArray[index];
            heapArray[index] = heapArray[largest];
            heapArray[largest] = temp;

            maxHeapify(largest);
        }
    }

    public void minHeapify(int i) {

        int leftChild = getLeftChild(i);
        int rightChild = getRightChild(i);
        int smallest;

        if (leftChild < heapSize && heapArray[leftChild] < heapArray[i]) {
            smallest = leftChild;
        } else smallest = i;
        if (rightChild < heapSize && heapArray[rightChild] < heapArray[smallest]) {
            smallest = rightChild;
        }
        if (smallest != i) {
            int temp = heapArray[i];
            heapArray[i] = heapArray[smallest];
            heapArray[smallest] = temp;

            minHeapify(smallest);
        }
    }

    public void buildMaxHeap() {

        int notLeaf = ((heapSize - 1) / 2) - 1;

        for (int j = notLeaf; j >= 0; j--) {
            maxHeapify(j);
        }
    }

    public void buildMinHeap() {

        int notLeaf = ((heapSize - 1) / 2) - 1;

        for (int j = notLeaf; j >= 0; j--) {
            minHeapify(j);
        }
    }

    public int[] heapSort() {

        buildMaxHeap();

        while (heapSize > 1) {
            int temp = heapArray[heapSize - 1];
            setNode(heapSize - 1, heapArray[0]);
            setNode(0, temp);
            heapSize = heapSize - 1;
            maxHeapify(0);
        }
        return heapArray;
    }
}
