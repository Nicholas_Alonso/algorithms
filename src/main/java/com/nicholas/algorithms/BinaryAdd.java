package com.nicholas.algorithms;
/**
 * Takes two unsigned binary numbers in the form of integer lists
 * and returns their sum.
 *
 * @author Nicholas Alonso
 */

import java.util.ArrayList;
import java.util.List;

public class BinaryAdd {
    /**
     * @param binOne the first binary number to be added
     * @param binTwo the second binary number to be added
     * @return sum, the binary sum of binOne and binTwo
     */
    public static List<Integer> binAdd(List<Integer> binOne, List<Integer> binTwo) {

        List<Integer> sum = new ArrayList<>();
        int carry = 0;

        for (int i = binOne.size() - 1; i > 0; i--) {
            sum.add(0, binOne.get(i) + binTwo.get(i) + carry);
            if (sum.get(0) == 2) {
                carry = 1;
                sum.set(0, 0);
            } else if (sum.get(0) == 3) {
                carry = 1;
                sum.set(0, 1);
            } else carry = 0;
        }

        if (binOne.get(0) + binTwo.get(0) + carry < 2) {
            sum.add(0, binOne.get(0) + binTwo.get(0) + carry);
        } else {
            if (binOne.get(0) + binTwo.get(0) + carry == 3) {
                sum.add(0, 1);
            } else sum.add(0, 0);
            sum.add(0, 1);
        }
        return sum;
    }
}
