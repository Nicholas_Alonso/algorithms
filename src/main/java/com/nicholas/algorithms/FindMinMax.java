package com.nicholas.algorithms;

import java.util.List;

/**
 * Find the minimum or maximum element in a given list.
 *
 * @author Nicholas Alonso
 */

public class FindMinMax {
    /**
     * Find the minimum element in a given list
     *
     * @param listToSearch the list to search for the minimum
     * @param <T>          the type of elements in the given list
     * @return the minimum element of the list.
     */
    public static <T extends Comparable<? super T>> T minimum(List<T> listToSearch) {

        T min = listToSearch.get(0);

        for (int i = 1; i < listToSearch.size(); i++) {
            if (listToSearch.get(i).compareTo(min) < 0) {
                min = listToSearch.get(i);
            }
        }
        return min;
    }

    public static <T extends Comparable<? super T>> T maximum(List<T> listToSearch) {

        T max = listToSearch.get(0);

        for (int i = 1; i < listToSearch.size(); i++) {
            if (listToSearch.get(i).compareTo(max) > 0) {
                max = listToSearch.get(i);
            }
        }
        return max;
    }
}
