package com.nicholas.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple implementation of bucket sort that sorts numbers
 * between 0 and 1
 *
 * @author Nicholas Alonso
 */
public class BucketSort {
    /**
     * @param myList the list of numbers to be sorted
     * @return a sorted version of myList
     */
    public static List<Double> bSort(List<Double> myList) {

        List<List<Double>> buckets = new ArrayList<>(10);

        for (int i = 0; i < 10; i++) {
            buckets.add(new ArrayList<>());
        }

        for (int i = 0; i <= myList.size() - 1; i++) {
            int index = (int) (10 * myList.get(i));
            buckets.get(index).add(myList.get(i));
        }

        buckets.forEach(InsertionSort::iSort);

        List<Double> sortedList = new ArrayList<>();
        buckets.forEach(sortedList::addAll);

        return sortedList;
    }
}

