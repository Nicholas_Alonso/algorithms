package com.nicholas.algorithms;

/**
 * Finds the sub array of a given array whose elements have the highest
 * sum of all sub arrays of the given array.
 *
 * @author Nicholas Alonso
 */
public class MaxSubArray {
    /**
     * The array will be broken into left and right parts and
     * this method finds the maximum sub array that crosses from
     * the left sub array and the right sub array.
     *
     * @param myArray the given array
     * @param low     the first element of the array or sub-array
     * @param mid     the middle element of the array or sub-array
     * @param high    the last element of the array or sub-array
     * @return an array that has the the indices of first and last
     * elements in the max sub array that crosses the left and right
     * parts and its sum.
     */
    public static int[] findMaxCrossing(int[] myArray, int low, int mid, int high) {

        int[] maxCrossing = new int[3];
        int leftSum = -10000;
        int sum = 0;
        int maxLeft = low;

        for (int i = mid; i >= low; i--) {
            sum = sum + myArray[i];
            if (sum > leftSum) {
                leftSum = sum;
                maxLeft = i;
            }
        }

        int rightSum = -10000;
        sum = 0;
        int maxRight = high;

        for (int j = mid + 1; j <= high; j++) {
            sum = sum + myArray[j];
            if (sum > rightSum) {
                rightSum = sum;
                maxRight = j;
            }
        }

        maxCrossing[0] = maxLeft;
        maxCrossing[1] = maxRight;
        maxCrossing[2] = leftSum + rightSum;
        return maxCrossing;
    }

    /**
     * Recursively finds the max sub array of left and right sub arrays
     * testing first for the base case where the sub array has only one element.
     * In the base case an array similar to the one returned in findMaxCrossing
     * but since the sub array only contains one element high and low are the same
     * and the sum is equal to the element.
     * If the array has more than one element then it is divided at the middle and
     * findMaxSubArray is called recursively on the left and right sub arrays until
     * each sub array has only one element.
     * Then the sub arrays are combined and the max crossing is found.
     * Finally the largest of the left, right, and crossing sub arrays is returned.
     *
     * @param myArray the array whose max sub array will be found
     * @param low     the lowest index of the array or sub array
     * @param high    the highest index of the array or sub array
     * @return an array with the first and last indices of the maximum sub array
     * and the sum of the maximum sub array.
     */
    public static int[] findMaxSubArray(int[] myArray, int low, int high) {

        int[] maxSubArray = new int[3];
        int sum = 0;
        int mid;
        int[] left;
        int[] right;
        int[] cross;
        maxSubArray[0] = low;
        maxSubArray[1] = high;
        maxSubArray[2] = sum;

        if (high == low) {
            maxSubArray[0] = low;
            maxSubArray[1] = high;
            maxSubArray[2] = myArray[low];

            return maxSubArray;
        } else {
            mid = (high + low) / 2;

            left = findMaxSubArray(myArray, low, mid);
            int leftLow = left[0];
            int leftHigh = left[1];
            int leftSum = left[2];

            right = findMaxSubArray(myArray, mid + 1, high);
            int rightLow = right[0];
            int rightHigh = right[1];
            int rightSum = right[2];

            cross = findMaxCrossing(myArray, low, mid, high);
            int crossLow = cross[0];
            int crossHigh = cross[1];
            int crossSum = cross[2];

            if (leftSum >= rightSum && leftSum >= crossSum) {
                maxSubArray[0] = leftLow;
                maxSubArray[1] = leftHigh;
                maxSubArray[2] = leftSum;

                return maxSubArray;
            } else if (rightSum >= leftSum && rightSum >= crossSum) {
                maxSubArray[0] = rightLow;
                maxSubArray[1] = rightHigh;
                maxSubArray[2] = rightSum;

                return maxSubArray;
            } else {
                maxSubArray[0] = crossLow;
                maxSubArray[1] = crossHigh;
                maxSubArray[2] = crossSum;

                return maxSubArray;
            }
        }
    }
}
