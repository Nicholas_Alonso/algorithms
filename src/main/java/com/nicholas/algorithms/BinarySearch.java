package com.nicholas.algorithms;

import java.util.List;

/**
 * Searches for a given element in a sorted list by checking the
 * element at the middle and comparing it to the element its
 * searching for. Then the process repeats on the appropriate half
 * of the given list until then wanted element is found or it cannot
 * be in the list. *
 *
 * @author Nicholas Alonso
 */
public class BinarySearch {
    /**
     * @param numToSearch the element that is to be searched for in the list
     * @param myList      the sorted list that is to be searched
     * @param <T>         the type of elements in myList
     * @return the index of the given element in the list or -1 if it is not found
     */
    public static <T extends Comparable<? super T>> int binarySearch(T numToSearch, List<T> myList) {

        int low = 0;
        int high = myList.size() - 1;

        while (low <= high) {
            int index = (low + high) / 2;

            if (myList.get(index).equals(numToSearch)) {
                return index;
            }
            if (myList.get(index).compareTo(numToSearch) < 0) {
                low = index + 1;
            } else high = index - 1;
        }
        return -1;
    }
}
