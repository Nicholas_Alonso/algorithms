package com.nicholas.algorithms;

import java.util.ArrayList;

/**
 * **
 * Implements heap using an ArrayList instead of an Array.
 * Also has the methods necessary for a priority queue.
 *
 * @param <E> the type elements in the HeapList
 * @author Nicholas Alonso
 */
public class HeapList<E extends Comparable<? super E>> {

    private int heapSize;
    private ArrayList<E> heapList;

    public HeapList(ArrayList<E> myList) {
        for (E item : myList) {
            heapList.add(item);
            heapSize++;
        }
    }

    public HeapList() {
        heapList = new ArrayList<>();
    }

    public ArrayList<E> getHeapList() {
        return heapList;
    }

    public void removeNode(int index) {

        heapList.remove(index);
        heapSize--;
    }

    public void setNode(E value, int index) {
        heapList.set(index, value);
    }

    public int getLeftChild(int index) {
        return ((index + 1) * 2) - 1;
    }

    public int getRightChild(int index) {
        return (index + 1) * 2;
    }

    public int getParent(int index) {
        return (index - 1) / 2;
    }

    /**
     * Assumes that both subtrees of the given index are max heaps
     * but the element at the index itself might be smaller than its
     * children. If necessary the element at the index will move down
     * the heap until the subtree rooted at the given index is a max
     * heap.
     *
     * @param index the index at which maxHeapify will occur
     */
    public void maxHeapify(int index) {

        int leftChild = getLeftChild(index);
        int rightChild = getRightChild(index);
        int largest;

        if (leftChild < heapSize
                && heapList.get(leftChild).compareTo(heapList.get(index)) > 0) {
            largest = leftChild;
        } else largest = index;

        if (rightChild < heapSize
                && heapList.get(rightChild).compareTo(heapList.get(largest)) > 0) {
            largest = rightChild;
        }

        if (largest != index) {
            E temp = heapList.get(index);
            heapList.set(index, heapList.get(largest));
            heapList.set(largest, temp);

            maxHeapify(largest);
        }
    }

    public void minHeapify(int index) {

        int leftChild = getLeftChild(index);
        int rightChild = getRightChild(index);
        int smallest;

        if (leftChild < heapSize && heapList.get(leftChild).compareTo(heapList.get(index)) < 0) {
            smallest = leftChild;
        } else smallest = index;

        if (rightChild < heapSize && heapList.get(rightChild).compareTo(heapList.get(smallest)) < 0) {
            smallest = rightChild;
        }

        if (smallest != index) {
            E temp = heapList.get(index);
            heapList.set(index, heapList.get(smallest));
            heapList.set(smallest, temp);

            minHeapify(smallest);
        }
    }

    /**
     * Calls maxHeapify on every non-leaf element of the heap
     * starting with the one with the highest index and working
     * backwards in order to build a max heap out of any heap.
     */
    public void buildMaxHeap() {

        int notLeaf = ((heapSize - 1) / 2) - 1;

        for (int j = notLeaf; j >= 0; j--) {
            maxHeapify(j);
        }
    }

    public void buildMinHeap() {

        int notLeaf = ((heapSize - 1) / 2) - 1;

        for (int j = notLeaf; j >= 0; j--) {
            minHeapify(j);
        }
    }

    /**
     * First uses buildMaxHeap ensuring that the element at
     * index 0 is the largest in the heap. Then swaps the
     * largest element with the last element in the heap and
     * decreases heapSize by one which ensures that when
     * maxHeapify is called on the new first element of the
     * heap the largest element will be ignored. This process
     * repeats, continuously putting the next largest element
     * in order at the end of the ArrayList until it is
     * completely sorted.
     *
     * @return heapList which will be sorted.
     */
    public ArrayList<E> heapListSort() {

        buildMaxHeap();

        while (heapSize > 1) {
            E temp = heapList.get(heapSize - 1);
            heapList.set(heapSize - 1, heapList.get(0));
            heapList.set(0, temp);
            heapSize--;
            maxHeapify(0);
        }

        return heapList;
    }

    public E getTop() {
        return heapList.get(0);
    }

    /**
     * Assumes that the given heap is a max heap.
     * Stores the element at index 0 of the heap then
     * sets the first element of the heap equal to the
     * last and removes the last element of the heap.
     * Finally calls maxHeapify to restore the max heap
     * property and returns the value that was stored.
     *
     * @return top, the element at the the top of the heap.
     */
    public E extractMax() {

        E top = heapList.get(0);
        heapList.set(0, heapList.get(heapSize - 1));
        removeNode(heapSize - 1);
        maxHeapify((0));
        return top;
    }

    public E extractMin() {

        E top = heapList.get(0);
        heapList.set(0, heapList.get(heapSize - 1));
        removeNode(heapSize - 1);
        minHeapify((0));
        return top;
    }

    /**
     * Increases the element at the given index to the given
     * key. Then repeatedly compares that element with its
     * parent making swaps when necessary until the max heap
     * property is restored.
     *
     * @param index the index at which the given key will be inserted
     * @param key   the element which will be swapped with the  element at
     *              the given index
     */
    public void heapIncreaseKey(int index, E key) {

        if (key.compareTo(heapList.get(index)) < 0) {
            System.out.println("The new key is less than the current one.");
        } else {
            heapList.set(index, key);

            while (index > 0 && heapList.get(index).compareTo(heapList.get(getParent(index))) > 0) {
                E temp = heapList.get(getParent(index));
                heapList.set(getParent(index), heapList.get(index));
                heapList.set(index, temp);
                index = getParent(index);
            }
        }
    }

    public void heapDecreaseKey(int index, E key) {

        if (key.compareTo(heapList.get(index)) > 0) {
            System.out.println("The new key is greater than the current one.");
        } else {
            heapList.set(index, key);

            while (index > 0 && heapList.get(index).compareTo(heapList.get(getParent(index))) < 0) {
                E temp = heapList.get(getParent(index));
                heapList.set(getParent(index), heapList.get(index));
                heapList.set(index, temp);
                index = getParent(index);
            }
        }
    }

    /**
     * Adds an element to the end of the heap then calls
     * heapIncreaseKey on it in order to have it move to
     * the correct location in the heap maintaining the
     * max heap property.
     *
     * @param key the element which is inserted into the heap
     */
    public void maxHeapInsert(E key) {

        heapList.add(key);
        heapSize++;
        heapIncreaseKey(heapSize - 1, key);
    }

    public void minHeapInsert(E key) {

        heapList.add(key);
        heapSize++;
        heapDecreaseKey(heapSize - 1, key);
    }

    /**
     * Swaps the element at the given index with the last element
     * of the heap. Then removes the last element of the heap and
     * finally calls maxHeapify on the given index to restore the
     * max heap property ultimately removing the element at the
     * given index while maintaining the max heap property.
     *
     * @param index the index of the element to be deleted from the heap
     */
    public void maxHeapDelete(int index) {

        setNode(heapList.get(heapSize - 1), index);
        removeNode(heapSize - 1);
        maxHeapify(index);
    }

    public void minHeapDelete(int index) {

        setNode(heapList.get(heapSize - 1), index);
        removeNode(heapSize - 1);
        minHeapify(index);
    }
}
