package com.nicholas.algorithms;

import java.util.List;

/**
 * Sorts a list by dividing into to sub lists, one is the sub list of
 * sorted elements which starts as an empty list and is located at the
 * beginning of the list, and the other is the list of unsorted elements.
 * The smallest element of the unsorted list is selected and then moved
 * to the end of the sorted list until the unsorted list has only one
 * element and therefore the entire list is sorted.
 *
 * @author Nicholas Alonso
 */
public class SelectionSort {
    /**
     * @param listToSort The list that is sorted
     * @param <T>        The type of elements in listToSort
     * @return The sorted List
     */
    public static <T extends Comparable<? super T>> List<T> sSort(List<T> listToSort) {

        int j = listToSort.size();

        for (int i = 0; i < j - 1; i++) {
            T temp = listToSort.get(i);
            int currentIndex = i;
            T currentLow = listToSort.get(i);

            for (int k = i + 1; k < j; k++) {
                if (listToSort.get(k).compareTo(currentLow) < 0) {

                    currentIndex = k;
                    currentLow = listToSort.get(k);
                }
            }
            listToSort.set(i, currentLow);
            listToSort.set(currentIndex, temp);
        }
        return listToSort;
    }
}
