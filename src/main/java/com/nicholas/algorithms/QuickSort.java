package com.nicholas.algorithms;

import java.util.List;

/**
 * Sorts a list by recursively partitioning it so that the left half is made up of elements
 * less than the element in the middle and the right half is made up of elements greater than
 * the element in the middle.
 */
public class QuickSort {
    /**
     * Returns q sort of the given list
     *
     * @param listToSort the list that is to be sorted
     * @param <T>        the type of elements in listToSort
     * @return the sorted list
     */
    public static <T extends Comparable<? super T>> List<T> quickSort(List<T> listToSort) {
        return qSort(listToSort, 0, listToSort.size() - 1);
    }

    /**
     * Uses the partition method and recursive calls to the left and right halves of listToSort
     * to sort it.
     *
     * @param listToSort The list that will be sorted
     * @param p          The lowest index of the list or sub list
     * @param r          The highest index of the list or sub list
     * @param <T>        The type of elements in the list
     * @return The list
     */
    public static <T extends Comparable<? super T>> List<T> qSort(List<T> listToSort, int p, int r) {

        if (p < r) {
            int q = partition(listToSort, p, r);
            qSort(listToSort, p, q - 1);
            qSort(listToSort, q + 1, r);
        }
        return listToSort;
    }

    /**
     * Changes the order of the list so that all elements
     * smaller than the element at index r are moved to the
     * left side of the list and then the element at index r
     * is swapped with the first element in the list that is
     * not less than it.
     *
     * @param listToSort The list that will be sorted
     * @param p          The lowest index of the list or sub list
     * @param r          The highest index of the list or sub list
     * @param <T>        The type of elements in the list
     * @return The index of the element that the list was partitioned
     * around
     */
    public static <T extends Comparable<? super T>> int partition(List<T> listToSort, int p, int r) {

        T x = listToSort.get(r);
        int i = p - 1;

        for (int j = p; j < r; j++) {
            if (listToSort.get(j).compareTo(x) < 0) {
                i++;
                T temp = listToSort.get(i);
                listToSort.set(i, listToSort.get(j));
                listToSort.set(j, temp);
            }
        }
        T temp2 = listToSort.get(i + 1);
        listToSort.set(i + 1, listToSort.get(r));
        listToSort.set(r, temp2);

        return i + 1;
    }
}
