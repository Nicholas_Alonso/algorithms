package com.nicholas.algorithms;

import java.util.List;

/**
 * Sorts small lists of numbers quickly but is slow when sorting
 * large lists.
 *
 * @author Nicholas Alonso
 */
public class InsertionSort {
    /**
     * @param listToSort the list of numbers to be sorted
     * @param <T>        the type of numbers in the list
     * @return the sorted list
     */
    public static <T extends Comparable<? super T>> List<T> iSort(List<T> listToSort) {

        for (int j = 1; j < listToSort.size(); j++) {
            int i = j - 1;
            T key = listToSort.get(j);

            while (i >= 0 && listToSort.get(i).compareTo(key) > 0) {
                listToSort.set(i + 1, listToSort.get(i));
                listToSort.set(i, key);
                i--;
            }
        }
        return listToSort;
    }
}
