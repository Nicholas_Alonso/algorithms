package com.nicholas.algorithms;

import java.util.List;

/**
 * Searches a list for a given element
 *
 * @author Nicholas Alonso
 */
public class Searching {
    /**
     * Searches a list for a given value in linear time by comparing each element of
     * the list to the value until there is a match or the entire list has been searched.
     *
     * @param value        The value that is searched for
     * @param listToSearch The list that is searched
     * @param <T>          The type of elements in the list
     * @return The index of the element searched for or -1 if it is not in the list
     */
    public static <T extends Comparable<? super T>> int search(T value, List<T> listToSearch) {

        for (int i = 0; i < listToSearch.size(); i++) {
            if (value.compareTo(listToSearch.get(i)) == 0) {
                return i;
            }
        }
        return -1;
    }
}
