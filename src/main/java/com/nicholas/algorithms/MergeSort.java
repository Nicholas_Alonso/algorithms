package com.nicholas.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * A comparison sort with an average run time of n lg n
 */
public class MergeSort {
    /**
     * Returns mSort of a given list
     *
     * @param listToSort the given list that is to be sorted
     * @param <T>        the type of elements in the given list
     * @return a sorted list
     */
    public static <T extends Comparable<? super T>> List<T> mergeSort(List<T> listToSort) {
        return mSort(listToSort, 0, listToSort.size() - 1);
    }

    /**
     * Divides a given list into left and right halves then recursively calls mSort on each half.
     * When the list only has one element it is returned. Then the left and right lists are merged
     * using the merge method which combines the two lists in increasing order.
     *
     * @param listToSort
     * @param p          the lowest index of the list or sub list
     * @param r          the highest index of the list or sub list
     * @param <T>        the type of elements in listToSort
     * @return the sorted list
     */
    public static <T extends Comparable<? super T>> List<T> mSort(List<T> listToSort, int p, int r) {

        if (p < r) {
            int q;
            q = (p + r) / 2;
            mSort(listToSort, p, q);
            mSort(listToSort, q + 1, r);
            merge(listToSort, p, q, r);
        }
        return listToSort;
    }

    /**
     * Creates lists left and right which are sub lists of listToSort then places the elements of the two
     * sub lists in order in listToSort from indices p to r.
     *
     * @param listToSort the list to be sorted
     * @param p          the lowest index of the array or sub array
     * @param q          (p + r) / 2
     * @param r          the highest index of the array
     * @param <T>        the type of elements in listToSort
     * @return the elements from indices p to r are sorted in place
     */
    public static <T extends Comparable<? super T>> List<T> merge(List<T> listToSort, int p, int q, int r) {

        int n1 = q - p + 1;
        int n2 = r - q;
        List<T> left = new ArrayList<>();
        List<T> right = new ArrayList<>();

        for (int i = 0; i < n1; i++) {
            left.add(i, listToSort.get(p + i));
        }

        for (int j = 0; j < n2; j++) {
            right.add(j, listToSort.get(q + j + 1));
        }

        int i = 0;
        int j = 0;

        for (int k = p; k <= r; k++) {
            if (i == n1) {
                while (k <= r) {
                    listToSort.set(k, right.get(j));
                    j++;
                    k++;
                }
            } else if (j == n2) {
                while (k <= r) {
                    listToSort.set(k, left.get(i));
                    i++;
                    k++;
                }
            } else {
                if (left.get(i).compareTo(right.get(j)) < 0) {
                    listToSort.set(k, left.get(i));
                    i++;
                } else {
                    listToSort.set(k, right.get(j));
                    j++;
                }
            }
        }
        return listToSort;
    }

}
