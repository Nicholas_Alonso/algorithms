package com.nicholas.algorithms;

/**
 * A stable sort in linear time.
 *
 * @author Nicholas Alonso
 */
public class CountingSort {
    /**
     * Sorts an array by counting the number of elements of each value
     * from 0 to max then placing each one into the index of a separate
     * array that is equal to the number of elements less than it.
     *
     * @param arrayToSort the array to be sorted
     * @param sortedArray the sorted array
     * @param max         the highest integer in arrayToSort
     */
    public static void countingSort(int[] arrayToSort, int[] sortedArray, int max) {

        int[] count = new int[max + 1];

        for (int index = 0; index <= max; index++) {
            count[index] = 0;
        }

        for (int index = 0; index < arrayToSort.length; index++) {
            count[arrayToSort[index]] = count[arrayToSort[index]] + 1;
        }

        for (int index = 1; index <= max; index++) {
            count[index] = count[index] + count[index - 1];
        }

        for (int index = arrayToSort.length - 1; index >= 0; index--) {
            sortedArray[count[arrayToSort[index]] - 1] = arrayToSort[index];
            count[arrayToSort[index]] = count[arrayToSort[index]] - 1;
        }
    }
}